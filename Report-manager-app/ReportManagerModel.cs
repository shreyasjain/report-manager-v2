﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report_manager_app
{
    public class InstanceModel
    {
        public int InstanceID { get; set; }
        public string InstanceName { get; set; }
        public string ConnectionString { get; set; }
        public string TemplatePath { get; set; }
        public string TempPath { get; set; }
        public string ImageRoutePath { get; set; }
        public string ReportOutputPath { get; set; }
        public string Status { get; set; }
        public string Message { get; set; }
    }
    public class ConfigModel
    {
        public List<InstanceModel> Instances { get; set; }
    }
}
