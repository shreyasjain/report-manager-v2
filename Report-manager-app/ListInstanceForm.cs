﻿using ReportManager;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Report_manager_app
{
    public partial class ListInstanceForm : Form
    {

        public ListInstanceForm()
        {
            InitializeComponent();

        }

        private void AddInstanceButton_Click(object sender, EventArgs e)
        {
            AddInstanceForm addInstanceForm = new AddInstanceForm(this);
            addInstanceForm.ShowDialog();
        }

        public void BindGrid()
        {
            Common cs = new Common();
            ConfigModel configModel = cs.ReadConfig();
            grdInstances.AutoGenerateColumns = false;
            grdInstances.DataSource = configModel.Instances.ToList();
            grdInstances.ClearSelection();
            hideButtons();
        }

        private void ListInstanceForm_Resize(object sender, EventArgs e)
        {
            if (FormWindowState.Minimized == WindowState)
            {
                Hide();
                ShowBallonTip();
                notifyIcon1.Visible = true;

            }
        }



        private void addInstanceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //AddInstanceForm rpmgr = new AddInstanceForm();
            //rpmgr.MdiParent = this;
            //rpmgr.Show();
            AddInstanceForm addInstanceForm = new AddInstanceForm(this);
            addInstanceForm.ShowDialog();
        }

        private void grdInstances_RowHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {

            string id = grdInstances.SelectedCells[0].Value.ToString();
            Common cs = new Common();
            ConfigModel configModel = cs.ReadConfig();
            InstanceModel instanceModel = configModel.Instances.Where(x => x.InstanceID == Convert.ToInt32(id)).FirstOrDefault();
            if (instanceModel.Status != InstanceStatus.Started)
            {
                AddInstanceForm addInstanceForm = new AddInstanceForm(this);
                addInstanceForm.BindData(Convert.ToInt32(id));
                addInstanceForm.ShowDialog();
            }
            else
            {
                MessageBox.Show("Can't edit a instance while it is " + InstanceStatus.Started + ", Please stop the process before editing.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }


        private void btnStart_Click(object sender, EventArgs e)
        {
            try
            {

                using (EventLog eventLog = new EventLog("Application"))
                {
                    eventLog.Source = "Application";
                    eventLog.WriteEntry("Inside Start", EventLogEntryType.SuccessAudit, 101, 1);
                }
                if (grdInstances.SelectedRows.Count == 1)
                {

                    int selectedrowindex = grdInstances.SelectedCells[0].RowIndex;
                    DataGridViewRow selectedRow = grdInstances.Rows[selectedrowindex];
                    int id = Convert.ToInt32(selectedRow.Cells["InstanceID"].Value);
                    Common cs = new Common();
                    ConfigModel configModel = cs.ReadConfig();
                    InstanceModel instanceModel = configModel.Instances.Where(x => x.InstanceID == id).FirstOrDefault();
                    

                    ReportService reportService = new ReportService(instanceModel);
                    if (reportService.ConnectDB())
                    {
                        using (EventLog eventLog = new EventLog("Application"))
                        {
                            eventLog.Source = "Application";
                            eventLog.WriteEntry("Starting service", EventLogEntryType.SuccessAudit, 101, 1);
                        }
                        reportService.StartReportService();
                        Common.UpdateInstanceStatus(InstanceStatus.Started, "Success", id);

                    }
                    else
                    {
                        Common.UpdateInstanceStatus(InstanceStatus.Stopped, "Can't Connect to database.", id);

                    }
                    BindGrid();
                }
            }
            catch (Exception ex)
            {

                using (EventLog eventLog = new EventLog("Application"))
                {
                    eventLog.Source = "Application";
                    eventLog.WriteEntry("Log message example " + ex.InnerException + " TRACE " + ex.Message, EventLogEntryType.Error, 101, 1);
                }
            }
        }

        private void btnStop_Click(object sender, EventArgs e)
        {

            if (grdInstances.SelectedRows.Count == 1)
            {

                int selectedrowindex = grdInstances.SelectedCells[0].RowIndex;
                DataGridViewRow selectedRow = grdInstances.Rows[selectedrowindex];
                int id = Convert.ToInt32(selectedRow.Cells["InstanceID"].Value);
                Thread thread = null;
                if (GlobalVariables.threadDictionary.TryGetValue("EMAIL_" + id.ToString(), out thread))
                {
                    if (thread.Name == id.ToString())
                    {
                        GlobalVariables.threadDictionary["PROCESS_" + id].Abort();
                        GlobalVariables.threadDictionary["EMAIL_" + id].Abort();


                        GlobalVariables.threadDictionary.Remove("EMAIL_" + InstanceID.ToString());
                        GlobalVariables.threadDictionary.Remove("PROCESS_" + InstanceID.ToString());
                    }
                }

                Common.UpdateInstanceStatus(InstanceStatus.Stopped, "Success", id);
                BindGrid();
            }
        }

        private void ListInstanceForm_Load(object sender, EventArgs e)
        {
            Common cs = new Common();
            ConfigModel configModel = cs.ReadConfig();
            List<InstanceModel> instanceModels = configModel.Instances.Where(x => x.Status == InstanceStatus.Started).ToList() ;
            foreach (var item in instanceModels)
            {
                ReportService reportService = new ReportService(item);
                if (reportService.ConnectDB())
                {
                    using (EventLog eventLog = new EventLog("Application"))
                    {
                        eventLog.Source = "Application";
                        eventLog.WriteEntry("Starting service", EventLogEntryType.SuccessAudit, 101, 1);
                    }
                    reportService.StartReportService();
                    Common.UpdateInstanceStatus(InstanceStatus.Started, "Success", item.InstanceID);

                }
                else
                {
                    Common.UpdateInstanceStatus(InstanceStatus.Stopped, "Can't Connect to database.", item.InstanceID);

                }
            }

            




            BindGrid();
            this.btnDelete.Enabled = false;
            this.btnStart.Enabled = false;
            this.btnStop.Enabled = false;
            this.btnRestart.Enabled = false;


            //lblVersion.Text = "V." + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();

        }


        private void grdInstances_RowStateChanged(object sender, DataGridViewRowStateChangedEventArgs e)
        {
            if (e.StateChanged == DataGridViewElementStates.Selected)
            {

                int id = Convert.ToInt32(e.Row.Cells["InstanceID"].Value);
                HideShowRowButton(id);
            }
        }
        public void hideButtons()
        {
            this.btnDelete.Enabled = false;
            this.btnStart.Enabled = false;
            this.btnStop.Enabled = false;
            this.btnRestart.Enabled = false;
        }
        public void HideShowRowButton(int id)
        {
            this.btnDelete.Enabled = false;
            this.btnStart.Enabled = false;
            this.btnStop.Enabled = false;
            this.btnRestart.Enabled = false;
            Common cs = new Common();
            ConfigModel configModel = cs.ReadConfig();
            InstanceModel instanceModel = configModel.Instances.Where(x => x.InstanceID == id).FirstOrDefault();
            if (instanceModel != null && instanceModel.Status.ToLower() == InstanceStatus.Started.ToLower())
            {
                btnStart.Enabled = false;
            }
            else
            {
                btnStart.Enabled = true;

            }
            if (instanceModel != null && instanceModel.Status.ToLower() == InstanceStatus.Stopped.ToLower())
            {
                btnStop.Enabled = false;
            }
            else
            {
                btnStop.Enabled = true;
            }
            if (instanceModel != null && instanceModel.Status.ToLower() != InstanceStatus.Started.ToLower())
            {
                btnRestart.Enabled = false;
            }
            else
            {
                btnRestart.Enabled = true;
            }

            if (instanceModel != null && instanceModel.Status.ToLower() == InstanceStatus.Stopped.ToLower())
            {
                btnDelete.Enabled = true;
            }
            else
            {
                btnRestart.Enabled = false;
            }

        }
        private void btnRestart_Click(object sender, EventArgs e)
        {
            btnStop_Click(sender, e);
            btnStart_Click(sender, e);
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {

            var confirmResult = MessageBox.Show("Are you sure to close this application?",
                                     "Confirm Exit!",
                                     MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            if (confirmResult == DialogResult.Yes)
            {
                Application.Exit();
            }
            else
            {
                // If 'No', do something here.
            }

        }
        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);

            if (e.CloseReason == CloseReason.ApplicationExitCall || e.CloseReason == CloseReason.WindowsShutDown) return;
            e.Cancel = true;
            // Confirm user wants to close
            WindowState = FormWindowState.Minimized;

            ShowBallonTip();
        }
        public void ShowBallonTip()
        {
            notifyIcon1.BalloonTipTitle = "eValuate Next";
            notifyIcon1.BalloonTipText = "Instances are running in background";
            notifyIcon1.BalloonTipIcon = ToolTipIcon.Info;
            notifyIcon1.ShowBalloonTip(2000);
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            var confirmResult = MessageBox.Show("Are you sure to delete this instance?",
                                  "Confirm Delete!",
                                  MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            if (confirmResult == DialogResult.Yes)
            {
                int selectedrowindex = grdInstances.SelectedCells[0].RowIndex;
                DataGridViewRow selectedRow = grdInstances.Rows[selectedrowindex];
                int id = Convert.ToInt32(selectedRow.Cells["InstanceID"].Value);
                Common cs = new Common();
                ConfigModel configModel = cs.ReadConfig();
                int currIndex = configModel.Instances.FindIndex(x => x.InstanceID == id);
                if (currIndex >= 0)
                {
                    configModel.Instances.RemoveAt(currIndex);
                    cs.UpdateConfig(configModel);
                }


                BindGrid();
            }
            else
            {
                // If 'No', do something here.
            }






        }



        private void grdInstances_RowEnter(object sender, DataGridViewCellEventArgs e)
        {

            DataGridViewRow selectedRow = grdInstances.Rows[e.RowIndex];
            int id = Convert.ToInt32(selectedRow.Cells["InstanceID"].Value);

            HideShowRowButton(id);
        }

        private void notifyIcon1_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                MethodInfo methodInfo = typeof(NotifyIcon).GetMethod("ShowContextMenu",
                    BindingFlags.Instance | BindingFlags.NonPublic);

                methodInfo.Invoke(this.notifyIcon1, null);
            }
        }


        private void openToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            this.Show();
            this.WindowState = FormWindowState.Normal;
            notifyIcon1.Visible = false;
        }

        private void toolStripExit_Click(object sender, EventArgs e)
        {
            var confirmResult = MessageBox.Show("Are you sure to close this application?",
                                  "Confirm Exit!",
                                  MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            if (confirmResult == DialogResult.Yes)
            {
                Application.Exit();
            }
            else
            {
                // If 'No', do something here.
            }
        }
    }
}
