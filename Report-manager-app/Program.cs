﻿using ReportManager;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Report_manager_app
{
    static class Program
    {
        /// <s
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Common cs = new Common();

            
            ConfigModel configModel = cs.ReadConfig();
            foreach (var item in configModel.Instances)
            {
               // CreateThreads(item);
            }

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new ListInstanceForm());


        }

        public static void CreateThreads(InstanceModel instance)
        {
            ReportService service = new ReportService(instance);

            if (service.ConnectDB())
            {
                Common.UpdateInstanceStatus(InstanceStatus.Ideal, "Success", instance);
                service.StartReportService();

            }
            else
            {
               //Common.UpdateInstanceStatus("Stopped","Could not open the database connection", instance);

            }
        }

    }
}
