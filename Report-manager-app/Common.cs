﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Report_manager_app
{
    static class InstanceStatus
    {
        public const string Started = "Running";
        public const string Stopped = "Stopped";
        public const string Ideal = "Ideal";
    }
    static class GlobalVariables
    {

        public static Dictionary<string, Thread> threadDictionary = new Dictionary<string, Thread>();
    }
    class Common
    {
        public static void UpdateInstanceStatus(string Status, string Message, InstanceModel instance)
        {
            Common cs = new Common();
            ConfigModel config = cs.ReadConfig();
            instance.Status = Status;
            instance.Message = Message;

            int currIndex = config.Instances.FindIndex(ind => ind.InstanceID == instance.InstanceID);
            config.Instances.RemoveAt(currIndex);
            config.Instances.Insert(currIndex, instance);

            cs.UpdateConfig(config);
        }
        public static void UpdateInstanceStatus(string Status, string Message, int instanceID)
        {
            try
            {
                Common cs = new Common();
                ConfigModel config = cs.ReadConfig();

                InstanceModel instance = config.Instances.Where(x => x.InstanceID == instanceID).FirstOrDefault();
                instance.Status = Status;
                instance.Message = Message;
                int currIndex = config.Instances.FindIndex(x => x.InstanceID == instance.InstanceID);

                config.Instances.RemoveAt(currIndex);
                config.Instances.Insert(currIndex, instance);

                cs.UpdateConfig(config);
            }
            catch (Exception e)
            {
                EventLog.WriteEntry("IN Application Exception Create", e.Message + "Trace" + e.StackTrace, EventLogEntryType.Error, 121, short.MaxValue);

            }

        }
        public ConfigModel ReadConfig()
        {
            try
            {
                ConfigModel result = new ConfigModel();
                string text;

                string path = ConfigurationManager.AppSettings["ConfigData"].ToString();

                var fileStream = new FileStream(path, FileMode.Open, FileAccess.Read);
                using (var streamReader = new StreamReader(fileStream, Encoding.UTF8))
                {
                    text = streamReader.ReadToEnd();
                    if (text != "")
                    {
                        result = JsonConvert.DeserializeObject<ConfigModel>(text);
                    }
                }
                if (result.Instances == null)
                    result.Instances = new List<InstanceModel>();
                return result;
            }
            catch (Exception e)
            {

                EventLog.WriteEntry("IN Application Exception Create", e.Message + "Trace" + e.StackTrace, EventLogEntryType.Error, 121, short.MaxValue);
                return null;
            }
        }
        public void UpdateConfig(ConfigModel configModel)
        {
            string path = ConfigurationManager.AppSettings["ConfigData"].ToString();
            //var fileStream = new FileStream(@"D:\SourceCode\Report-manager-app\Report-manager-app\ReportManager.json", FileMode.Open, FileAccess.ReadWrite);
            using (var streamWriter = new StreamWriter(path, false))
            {
                streamWriter.Write(JsonConvert.SerializeObject(configModel));
                streamWriter.Close();
                streamWriter.Dispose();

            }
        }



    }
}
