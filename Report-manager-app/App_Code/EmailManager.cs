﻿using System;
using System.Configuration;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Net.Mail;
using System.Net.Mime;
using System.Net;

namespace UtilityBox
{
    static class EmailManager
    {
        static SmtpClient smtpclient;

        static EmailManager()
        {
            NameValueCollection emailConfiguration;
            //string username = "", password = "";

            //emailConfiguration = ConfigurationManager.GetSection("EmailConfiguration") as NameValueCollection;
            //smtpclient = new SmtpClient(emailConfiguration["Host"].ToString(), Convert.ToInt32(emailConfiguration["Port"]));


            //if (emailConfiguration["Password"] != null) password = emailConfiguration["Password"].ToString();
            //if (emailConfiguration["SSLEnabled"] != null) smtpclient.EnableSsl = Convert.ToBoolean(emailConfiguration["SSLEnabled"]);

            //if (username != "")
            //    smtpclient.Credentials = new System.Net.NetworkCredential(username, password);

            smtpclient = new SmtpClient();
        }

        public static MailMessage NewMessage()
        {

            return new MailMessage();
        }

        public static void Send(MailMessage msg)
        {
            smtpclient.Timeout = 100000;
            smtpclient.Send(msg);
        }
    }
}
