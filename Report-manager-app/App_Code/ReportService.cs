﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Mail;
using System.Threading;
using System.Threading.Tasks;

using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Tables;
using MigraDoc.Rendering;
using Newtonsoft.Json.Linq;
using PdfSharp.Pdf;
using UtilityBox;
using Novacode = Xceed.Words.NET;
using Newtonsoft.Json;
using Report_manager_app;
using System.Data.Common;
using System.Diagnostics;

namespace ReportManager
{


    public class AssetReportFieldModel
    {
        public int AssetFieldID { get; set; }
        public string DefaultBehaviour { get; set; }
    }

    public class ReportFieldConfig
    {
        public string Evaluate { get; set; }
        public string Audit { get; set; }
        public string On { get; set; }
        public string At { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }

        public string SubAssetID { get; set; }
        public string SubAssetType { get; set; }
        public string SubAssetConditionRating { get; set; }
        public string SubAssetAreaLocated { get; set; }
        public string SubAssetMake { get; set; }
        public string SubAssetModel { get; set; }
        public string SubAssetSerial { get; set; }



    }
    public class AssetFieldListDetailsModel
    {
        public int AssetFieldID { get; set; }
        public int FieldGroupID { get; set; }
        public string GroupName { get; set; }
        public string FieldName { get; set; }
        public string DisplayName { get; set; }
        public string DefaultBehaviour { get; set; }
        public string Description { get; set; }
        public bool Mobile { get; set; }
        public bool Web { get; set; }
        public string DataType { get; set; }
        public string Length { get; set; }
        public string RegExpression { get; set; }
        public string DataSource { get; set; }
        public string ValueFields { get; set; }
        public string FieldType { get; set; }
        public bool IsReadonly { get; set; }
        public int Sequence { get; set; }
    }
    public class EmailTemplateEntityModel
    {
        public int EmailTemplateID { get; set; }
        public string Type { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public string DataFields { get; set; }

    }
    public class SubAssetDataModel
    {
        public string SubAssetID { get; set; }
        public string AssetID { get; set; }
        public string UserAssetID { get; set; }
        public string AssetTypeID { get; set; }
        public string AssetTypeName { get; set; }
        public string ConditionRatingCd { get; set; }
        public string ConditionRating { get; set; }
        public string AreaLocated { get; set; }
        public string Make { get; set; }
        public string Manufacturer { get; set; }
        public string Model { get; set; }
        public string SerialNo { get; set; }
        public string HeightLength { get; set; }
        public string WidthBreadth { get; set; }
        public string ThicknessDepth { get; set; }
    }
    public enum ServiceStatus
    {
        Start = 1,
        Stop = 2
    }
    //https://msdn.microsoft.com/en-us/library/zt39148a(v=vs.110).aspx
    public class ReportService
    {


        Thread reportThread;
        Thread emailThread;
        string ReportOutputPath;
        string ImageRootPath;
        string ReportURL;
        string TempPath;
        string Senderemail;
        string connectionString;
        string TemplatePath;
        int InstanceID;
        SqlConnection sqlConn;
        public ReportService(InstanceModel instanceModel)
        {
            //ReportOutputPath = ConfigurationManager.AppSettings["ReportOutputPath"];
            ImageRootPath = instanceModel.ImageRoutePath;
            //Mode = "Test";
            //ReportURL = instanceModel;
            Senderemail = ConfigurationManager.AppSettings["SenderEmail"];
            TemplatePath = instanceModel.TemplatePath;
            TempPath = instanceModel.TempPath;
            connectionString = instanceModel.ConnectionString;
            InstanceID = instanceModel.InstanceID;

            ReportOutputPath = instanceModel.ReportOutputPath;
            //ImageRootPath = ConfigurationManager.AppSettings["ImageRootPath"];
            //Mode = ConfigurationManager.AppSettings["Mode"];
            ReportURL = ConfigurationManager.AppSettings["ReportURL"].ToString();
            //Senderemail = ConfigurationManager.AppSettings["SenderEmail"];
            //TemplatePath = ConfigurationManager.AppSettings["TemplatePath"];
            //TempPath = ConfigurationManager.AppSettings["TempPath"];

            //connectionString = ConfigurationManager.ConnectionStrings["evaluateDB"].ConnectionString;


            //if (ReportOutputPath.Substring(ReportOutputPath.Length - 1) != "\\")
            //    ReportOutputPath = ReportOutputPath + "\\";
            //if (TemplatePath.Substring(TemplatePath.Length - 1) != "\\")
            //    TemplatePath = TemplatePath + "\\";


            //if (!Directory.Exists("Temp"))
            //{
            //    Directory.CreateDirectory("Temp");
            //}
        }


        public bool ConnectDB()
        {
            try
            {
                sqlConn = new SqlConnection(connectionString);
                sqlConn.Open();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public void StartReportService()
        {
            reportThread = new Thread(new ThreadStart(StartReportManager));

            reportThread.Name = InstanceID.ToString();
            emailThread = new Thread(new ThreadStart(StartEmailer));
            emailThread.Name = InstanceID.ToString();

            Thread thread = null;
            if (GlobalVariables.threadDictionary.TryGetValue("EMAIL_" + InstanceID.ToString(), out thread))
            {
                if (thread.Name == InstanceID.ToString())
                {

                    GlobalVariables.threadDictionary.Remove("EMAIL_" + InstanceID.ToString());
                    GlobalVariables.threadDictionary.Remove("PROCESS_" + InstanceID.ToString());
                }
            }

            GlobalVariables.threadDictionary.Add("EMAIL_" + InstanceID.ToString(), emailThread);
            GlobalVariables.threadDictionary.Add("PROCESS_" + InstanceID.ToString(), reportThread);

            reportThread.Start();
            emailThread.Start();
            using (EventLog eventLog = new EventLog("Application"))
            {
                eventLog.Source = "Application";
                eventLog.WriteEntry("Thread start", EventLogEntryType.SuccessAudit, 101, 1);
            }

        }

        private async Task<string> GenerateReport(string Format, int UserID, int CustomerID, int? LocationID, int? AssetReportID,
            string ConditionRating, string AssetStatus, string AssetCategory, string Criticality,
            string COwnership, string AssetType, string Refrigerant, string FiltersFitted,
            string ReplacementYear, string AgeGroup, string OHSComments, string RemainingLife,
            string ReplacementCost, string JobCount, string AssetFields, string State, string LanguageLitral)
        {

            string Reportfilename = "";
            //bool JobDataAllowed;

            //SqlCommand JobData;
            //JobData = sqlConn.CreateCommand();
            //JobData.CommandType = CommandType.Text;
            //JobData.CommandText = "Select IsJobDataAllowed(" + UserID.ToString() + " as JobAllowed)";
            //JobDataAllowed = (bool)JobData.ExecuteScalar();

            DataTable AssetData = new DataTable();

            try
            {
                SqlCommand ReportData;
                ReportData = sqlConn.CreateCommand();
                ReportData.CommandType = CommandType.StoredProcedure;
                ReportData.CommandText = "GetAssetReportDetails";
                //ReportData.Parameters.Add(new SqlParameter("UserID", SqlDbType.Int)).Value = UserID;
                ReportData.Parameters.Add(new SqlParameter("CustomerID", SqlDbType.Int)).Value = CustomerID;
                ReportData.Parameters.Add(new SqlParameter("AssetTypeID", SqlDbType.VarChar)).Value = AssetType;
                ReportData.Parameters.Add(new SqlParameter("LocationID", SqlDbType.Int)).Value = LocationID;
                ReportData.Parameters.Add(new SqlParameter("ConditionRating", SqlDbType.VarChar)).Value = ConditionRating;
                ReportData.Parameters.Add(new SqlParameter("Status", SqlDbType.VarChar)).Value = AssetStatus;
                ReportData.Parameters.Add(new SqlParameter("Criticality", SqlDbType.VarChar)).Value = Criticality;
                ReportData.Parameters.Add(new SqlParameter("AssetOwnership", SqlDbType.VarChar)).Value = COwnership;
                ReportData.Parameters.Add(new SqlParameter("FiltersFitted", SqlDbType.VarChar)).Value = FiltersFitted;
                ReportData.Parameters.Add(new SqlParameter("Refrigerant", SqlDbType.VarChar)).Value = Refrigerant;
                ReportData.Parameters.Add(new SqlParameter("AssetGroup", SqlDbType.VarChar)).Value = AssetCategory;
                ReportData.Parameters.Add(new SqlParameter("AgeGroup", SqlDbType.VarChar)).Value = AgeGroup;
                ReportData.Parameters.Add(new SqlParameter("RemainingLife", SqlDbType.VarChar)).Value = RemainingLife;
                ReportData.Parameters.Add(new SqlParameter("State", SqlDbType.VarChar)).Value = State;
                ReportData.Parameters.Add(new SqlParameter("ReplacementCost", SqlDbType.VarChar)).Value = ReplacementCost;

                SqlDataReader ReportDataReader = ReportData.ExecuteReader();
                AssetData.Load(ReportDataReader);

            }
            catch (Exception ex)
            {

                using (EventLog eventLog = new EventLog("Application"))
                {
                    eventLog.Source = "Application";
                    eventLog.WriteEntry("After  GetAssetReportDetails " + ex.InnerException, EventLogEntryType.SuccessAudit, 101, 1);
                }
            }



            //System.Diagnostics.EventLog.WriteEntry("Report Manager Service", "Reached here after Data", System.Diagnostics.EventLogEntryType.Information);
            //Filter data based on Customer
            AssetData = GetReportAssetData(UserID, CustomerID, AssetData, AssetFields, LanguageLitral);

            Task<string> task;
            //task = GenerateWordReport(AssetData);
            //Reportfilename = await task;

            switch (Format)
            {
                case "DOC":
                    task = GenerateWordReport(AssetData, LanguageLitral);
                    Reportfilename = await task;
                    break;
                case "PDF":
                    task = GeneratePDFReport(AssetData, LanguageLitral);
                    Reportfilename = await task;
                    break;
                    /*case "XLS":
                        task = GenerateXLSReport(AssetsColumns);
                        Reportfilename = await task;
                        break;*/
            }
            return Reportfilename;
        }

        private async Task<string> GeneratePDFReport(DataTable AssetData, string LanguageLitral)
        {
            List<AssetFieldListDetailsModel> readAssetFields = ReadAssetFields(LanguageLitral);

            string AssetID = readAssetFields.Where(x => x.FieldName == "UserAssetID").FirstOrDefault().DisplayName;
            string Location = readAssetFields.Where(x => x.FieldName == "AssetLocationID").FirstOrDefault().DisplayName;
            ReportFieldConfig reportFieldConfig = ReadReportFields(LanguageLitral);

            string ReportHeader = reportFieldConfig.Evaluate + " " + reportFieldConfig.Audit + " " + reportFieldConfig.On + " {0} " + reportFieldConfig.At + " {1}";

            const bool unicode = true;
            string Reportfilename; string Header;
            const PdfFontEmbedding embedding = PdfFontEmbedding.Always;
            Document document = new Document();

            document.DefaultPageSetup.HeaderDistance = Unit.FromCentimeter(0.8);
            document.DefaultPageSetup.FooterDistance = Unit.FromCentimeter(0.2);
            document.DefaultPageSetup.TopMargin = Unit.FromCentimeter(1);
            document.DefaultPageSetup.LeftMargin = Unit.FromCentimeter(2);
            document.DefaultPageSetup.RightMargin = Unit.FromCentimeter(1);
            document.DefaultPageSetup.BottomMargin = Unit.FromCentimeter(1.5);
            Style style = document.Styles["Normal"];
            style.Font.Name = "Arial Unicode MS";
            style.Font.Size = 9;
            style.ParagraphFormat.Alignment = ParagraphAlignment.Left;
            style.ParagraphFormat.SpaceBefore = 2;
            style.ParagraphFormat.SpaceAfter = 2;
            style = document.Styles[StyleNames.Header];
            style.ParagraphFormat.Font.Bold = true;
            style.ParagraphFormat.Font.Size = 10;
            style.ParagraphFormat.Alignment = ParagraphAlignment.Center;
            style.ParagraphFormat.SpaceAfter = Unit.FromCentimeter(1);
            style = document.Styles[StyleNames.Footer];
            style.ParagraphFormat.AddTabStop("50cm", MigraDoc.DocumentObjectModel.TabAlignment.Right);
            style.ParagraphFormat.Alignment = ParagraphAlignment.Left;
            //Create Temporary folder for downloading images
            //string TempimagesPath;
            //TempimagesPath = "Temp/" + "Images~";
            //Directory.CreateDirectory(TempimagesPath);
            MigraDoc.DocumentObjectModel.Section section = document.AddSection();
            var CoverImage = section.AddImage(TemplatePath + "/images/coverpage.jpg");
            CoverImage.Width = 470;
            CoverImage.Height = 700;
            //section = document.AddSection();
            MigraDoc.DocumentObjectModel.Paragraph footer = new MigraDoc.DocumentObjectModel.Paragraph();
            //footer.AddPageField();
            //section.Footers.Primary.Add(footer);
            //section.Footers.EvenPage.Add(footer.Clone());
            //footer = new Paragraph();

            footer.AddImage(TemplatePath + "/images/logo.png");
            section.Footers.Primary.Add(footer);
            foreach (DataRow record in AssetData.Rows)
            {
                section = document.AddSection();
                section.AddPageBreak();
                Header = string.Format(ReportHeader, record[AssetID].ToString(), record[Location].ToString());
                //Header = "eValuate Audit on " + record[AssetID].ToString() + " at " + record[Location].ToString();
                section.AddParagraph(Header).Style = StyleNames.Header;
                MigraDoc.DocumentObjectModel.Tables.Table DataTable = section.AddTable();
                DataTable.Borders.Width = 1;
                DataTable.Style = "TableStyle";
                //table.SetEdge(0, 0, 2, 3, Edge.Box, MigraDoc.DocumentObjectModel.BorderStyle.Single, 1.5, Colors.Black);
                Column column;
                MigraDoc.DocumentObjectModel.Tables.Row row;
                column = DataTable.AddColumn(Unit.FromInch(1.5));
                column = DataTable.AddColumn(Unit.FromInch(3));
                column = DataTable.AddColumn(Unit.FromInch(3.5));
                row = DataTable.AddRow();
                row.Format.Font.Bold = true;
                row.Cells[0].Borders.Width = 1;
                row.Cells[1].Borders.Width = 1;
                row.Cells[2].Borders.Width = 0;

                row.Cells[0].AddParagraph(reportFieldConfig.Name);
                row.Cells[1].AddParagraph(reportFieldConfig.Value);

                row.Cells[2].MergeDown = (AssetData.Columns.Count - 2 > 41 ? 41 : AssetData.Columns.Count - 2); //AssetsColumns.Count;

                row.Cells[2].Format.Alignment = ParagraphAlignment.Left;
                row.Cells[2].VerticalAlignment = MigraDoc.DocumentObjectModel.Tables.VerticalAlignment.Top;
                var paragraph = row.Cells[2].AddParagraph();
                paragraph.Format.Alignment = ParagraphAlignment.Center;

                List<string> images = AddImages("AST", Convert.ToInt32(record["AssetID"]));
                if (images != null)
                {
                    foreach (var image in images)
                    {

                        MigraDoc.DocumentObjectModel.Shapes.Image img = paragraph.AddImage(image);
                        paragraph.AddLineBreak();
                        paragraph.AddLineBreak();
                    }
                }
                //int FieldIndex = 0;
                foreach (DataColumn field in AssetData.Columns)
                {
                    //Columns skip
                    if ((field.ColumnName == "Customer Comments") || (field.ColumnName == "AssetID"))
                    {
                        continue;
                    }
                    row = DataTable.AddRow();
                    row.Cells[0].Borders.Width = 1;
                    row.Cells[1].Borders.Width = 1;
                    row.Cells[2].Borders.Width = 0;
                    row.Cells[0].Format.Font.Bold = true;
                    try
                    {
                        //row.Cells[0].Format.Font.Size = 8;
                        row.Cells[0].AddParagraph(Convert.ToString(field.ColumnName));
                        //  row.Cells[1].Paragraphs.First().Append(record[field].ToString());
                    }
                    catch
                    {
                        row.Cells[0].AddParagraph("N/A");
                    }
                    try
                    {
                        row.Cells[1].AddParagraph(record[field].ToString());
                    }
                    catch (Exception ex)
                    {
                        row.Cells[1].AddParagraph("N/A");
                    }
                    ///FieldIndex = FieldIndex + 1;
                }
                // Sub Asset Section 
                //sub Assets
                List<SubAssetDataModel> subAssetData = GetReportSubAssetData(Convert.ToInt32(record["AssetID"]));


                if (subAssetData != null)
                {
                    foreach (SubAssetDataModel sub in subAssetData)
                    {
                        section = document.AddSection();
                        section.AddPageBreak();
                        //Header = "eValuate Audit on " + sub.UserAssetID.ToString() + " at " + record[Location].ToString();
                        Header = reportFieldConfig.Evaluate + " " + reportFieldConfig.Audit + " " + reportFieldConfig.On + " " + sub.UserAssetID.ToString() + " " + reportFieldConfig.At + " " + record[Location].ToString();
                        section.AddParagraph(Header).Style = StyleNames.Header;
                        MigraDoc.DocumentObjectModel.Tables.Table SubDataTable = section.AddTable();
                        SubDataTable.Borders.Width = 1;
                        SubDataTable.Style = "TableStyle";

                        //MigraDoc.DocumentObjectModel.Tables.Row row;
                        column = SubDataTable.AddColumn(Unit.FromInch(1.5));
                        column = SubDataTable.AddColumn(Unit.FromInch(3));
                        column = SubDataTable.AddColumn(Unit.FromInch(3.5));

                        row = SubDataTable.AddRow();
                        row.Format.Font.Bold = true;
                        row.Cells[0].Borders.Width = 1;
                        row.Cells[1].Borders.Width = 1;
                        row.Cells[2].Borders.Width = 0;

                        row.Cells[0].AddParagraph(reportFieldConfig.Name);
                        row.Cells[1].AddParagraph(reportFieldConfig.Value);

                        row.Cells[2].MergeDown = 7;
                        row.Cells[2].Format.Alignment = ParagraphAlignment.Left;
                        row.Cells[2].VerticalAlignment = MigraDoc.DocumentObjectModel.Tables.VerticalAlignment.Top;
                        paragraph = row.Cells[2].AddParagraph();
                        paragraph.Format.Alignment = ParagraphAlignment.Center;

                        List<string> SubImages = AddImages("SAST", Convert.ToInt32(sub.SubAssetID));
                        if (SubImages != null)
                        {
                            //int imageIndex = 0;
                            foreach (var imag in SubImages)
                            {
                                MigraDoc.DocumentObjectModel.Shapes.Image img = paragraph.AddImage(imag);
                                paragraph.AddLineBreak();
                                paragraph.AddLineBreak();
                            }
                        }

                        row = SubDataTable.AddRow();
                        row.Cells[0].Borders.Width = 1;
                        row.Cells[1].Borders.Width = 1;
                        row.Cells[2].Borders.Width = 0;
                        row.Cells[0].Format.Font.Bold = true;



                        row.Cells[0].AddParagraph(reportFieldConfig.SubAssetID);
                        row.Cells[1].AddParagraph(sub.UserAssetID);

                        row = SubDataTable.AddRow();
                        row.Cells[0].AddParagraph(reportFieldConfig.SubAssetType);
                        row.Cells[1].AddParagraph(sub.AssetTypeName);
                        row.Cells[0].Format.Font.Bold = true;
                        row.Cells[2].Borders.Width = 0;

                        row = SubDataTable.AddRow();
                        row.Cells[0].AddParagraph(reportFieldConfig.SubAssetConditionRating);
                        row.Cells[1].AddParagraph(sub.ConditionRating);
                        row.Cells[0].Format.Font.Bold = true;
                        row.Cells[2].Borders.Width = 0;

                        row = SubDataTable.AddRow();
                        row.Cells[0].AddParagraph(reportFieldConfig.SubAssetAreaLocated);
                        row.Cells[1].AddParagraph(sub.AreaLocated);
                        row.Cells[0].Format.Font.Bold = true;
                        row.Cells[2].Borders.Width = 0;

                        row = SubDataTable.AddRow();
                        row.Cells[0].AddParagraph(reportFieldConfig.SubAssetMake);
                        row.Cells[1].AddParagraph(sub.Manufacturer);
                        row.Cells[0].Format.Font.Bold = true;
                        row.Cells[2].Borders.Width = 0;

                        row = SubDataTable.AddRow();
                        row.Cells[0].AddParagraph(reportFieldConfig.SubAssetModel);
                        row.Cells[1].AddParagraph(sub.Model);
                        row.Cells[0].Format.Font.Bold = true;
                        row.Cells[2].Borders.Width = 0;

                        row = SubDataTable.AddRow();
                        row.Cells[0].AddParagraph(reportFieldConfig.SubAssetSerial);
                        row.Cells[1].AddParagraph(sub.SerialNo);
                        row.Cells[0].Format.Font.Bold = true;
                        row.Cells[2].Borders.Width = 0;

                    }
                }// end of sub Assets
            }
            PdfDocumentRenderer pdfRenderer = new PdfDocumentRenderer(unicode, PdfFontEmbedding.Automatic);
            pdfRenderer.Document = document;
            pdfRenderer.RenderDocument();
            Reportfilename = "AssetReport" + DateTime.UtcNow.TimeOfDay.ToString().Replace(":", "").Replace("-", "").Replace(" ", "").Replace("/", "") + ".pdf";
            pdfRenderer.PdfDocument.Save(ReportOutputPath + Reportfilename);
            //Removing temporary files
            DeleteFiles(TempPath);
            return Reportfilename;
        }
        public async Task<string> GenerateWordReport(DataTable AssetData, string LanguageLitral)
        {
            List<AssetFieldListDetailsModel> readAssetFields = ReadAssetFields(LanguageLitral);

            string AssetID = readAssetFields.Where(x => x.FieldName == "UserAssetID").FirstOrDefault().DisplayName;
            string Location = readAssetFields.Where(x => x.FieldName == "AssetLocationID").FirstOrDefault().DisplayName;
            ReportFieldConfig reportFieldConfig = ReadReportFields(LanguageLitral);

            string ReportHeader = reportFieldConfig.Evaluate + " " + reportFieldConfig.Audit + " " + reportFieldConfig.On + " {0} " + reportFieldConfig.At + " {1}";

            //Create Temporary folder for downloading images
            string TempimagesPath;
            string tempFileName, ReportFileName;
            // Create a document in memory:
            using (Novacode.DocX document = Novacode.DocX.Create(@"ReportTemplate.docx"))
            {
                //Document settings
                document.AddHeaders();
                document.AddFooters();
                document.MarginTop = 60F;
                document.MarginRight = 25F;
                document.MarginLeft = 60F;
                document.MarginBottom = 25F;
                var NormalFormat = new Novacode.Formatting();
                NormalFormat.FontFamily = new Novacode.Font("Arial"); // " wnew System.Drawing.FontFamily("Arial");
                NormalFormat.Size = 9D;
                var HeaderFormat = new Novacode.Formatting();
                HeaderFormat.FontFamily = new Novacode.Font("Arial"); //System.Drawing.FontFamily("Arial");
                HeaderFormat.Size = 10D;
                HeaderFormat.Bold = true;
                //HeaderFormat.Spacing = 2;
                //Create Temporary folder for downloading images
                //TempimagesPath = "Temp/" + "Images~";

                //Cover Page section
                Novacode.Picture coverpicture = document.AddImage(TemplatePath + "/images/coverpage.jpg").CreatePicture();
                coverpicture.Width = 620;
                coverpicture.Height = 950;
                Novacode.Paragraph cover = document.InsertParagraph();
                cover.AppendPicture(coverpicture);
                //Footer Logo section
                Novacode.Picture logo = document.AddImage(TemplatePath + "/images/logo.png").CreatePicture();
                Novacode.Footer footer = document.Footers.Odd;
                Novacode.Paragraph pfooter = footer.InsertParagraph();
                pfooter.InsertPicture(logo);
                pfooter.Direction = Novacode.Direction.RightToLeft;

                document.InsertSectionPageBreak();

                foreach (DataRow record in AssetData.Rows)
                {
                    var text = string.Format(ReportHeader, record[AssetID].ToString(), record[Location].ToString());
                    Novacode.Paragraph header = document.InsertParagraph(text, false, HeaderFormat).AppendLine();
                    header.Alignment = Novacode.Alignment.center;
                    Novacode.Table DataTable = document.InsertTable(1, 3);
                    DataTable.Design = Novacode.TableDesign.TableGrid;
                    DataTable.Rows[0].Cells[0].Paragraphs.First().Append(reportFieldConfig.Name).Bold();
                    DataTable.Rows[0].Cells[1].Paragraphs.First().Append(reportFieldConfig.Value).Bold();
                    DataTable.Rows[0].Cells[0].Width = 150;
                    DataTable.Rows[0].Cells[1].Width = 270;
                    DataTable.Rows[0].Cells[2].Width = 250;

                    Novacode.Border border = new Novacode.Border(Novacode.BorderStyle.Tcbs_none, Novacode.BorderSize.one, 1, System.Drawing.Color.White);

                    List<string> images = AddImages("AST", Convert.ToInt32(record["AssetID"]));
                    DataTable.Rows[0].Cells[2].SetBorder(Novacode.TableCellBorderType.Top, border);
                    DataTable.Rows[0].Cells[2].SetBorder(Novacode.TableCellBorderType.Right, border);
                    DataTable.Rows[0].Cells[2].SetBorder(Novacode.TableCellBorderType.Bottom, border);

                    if (images != null)
                    {
                        //int imageIndex = 0;
                        foreach (var img in images)
                        {
                            Novacode.Paragraph paragraph = DataTable.Rows[0].Cells[2].InsertParagraph();
                            paragraph.Alignment = Novacode.Alignment.center;
                            Novacode.Image image = document.AddImage(img);
                            paragraph.InsertPicture(image.CreatePicture()).AppendLine().Alignment = Novacode.Alignment.right;
                            paragraph.Alignment = Novacode.Alignment.right;
                            paragraph.IndentationHanging = 1.0f;
                            paragraph.Direction = Novacode.Direction.RightToLeft;
                        }
                    }


                    foreach (DataColumn field in AssetData.Columns)
                    {
                        if ((field.ColumnName == "Customer Comments") || (field.ColumnName == "AssetID"))
                        {
                            continue;
                        }
                        //if (((item.DisplayName == "Job Count") || (item.DisplayName == "Total Spend") && !JobDataAllowed) ||
                        //(item.DisplayName == "Photos") || (item.DisplayName == "Asset ID") || (item.DisplayName == "Audit Start") || (item.DisplayName == "Audit End"))
                        //{
                        //    // Skipping System fields
                        //    continue;
                        //}
                        Novacode.Row row = DataTable.InsertRow();
                        row.Cells[0].Width = 150;
                        row.Cells[1].Width = 270;
                        row.Cells[2].Width = 250;

                        border = new Novacode.Border(Novacode.BorderStyle.Tcbs_none, Novacode.BorderSize.one, 1, System.Drawing.Color.White);

                        row.Cells[2].SetBorder(Novacode.TableCellBorderType.Top, border);
                        row.Cells[2].SetBorder(Novacode.TableCellBorderType.Right, border);
                        row.Cells[2].SetBorder(Novacode.TableCellBorderType.Bottom, border);

                        try
                        {
                            Novacode.Paragraph Label = row.Cells[0].Paragraphs.First();
                            Label.Append(Convert.ToString(field.ColumnName)).Bold();
                            //Label.UnderlineStyle. .= HeaderFormat;
                        }
                        catch
                        {
                            row.Cells[0].Paragraphs.First().Append("N/A");
                        }
                        //var fieldName = string.Empty;
                        try
                        {
                            //fieldName = Convert.ToString(field.ColumnName);
                            row.Cells[1].Paragraphs.First().Append(record[field].ToString());
                        }
                        catch
                        {
                            // fieldName = "N/A";
                            row.Cells[1].Paragraphs.First().Append("N/A");
                        }
                    }
                    DataTable.MergeCellsInColumn(2, 0, DataTable.RowCount - 1);

                    //sub Assets
                    List<SubAssetDataModel> subAssetData = GetReportSubAssetData(Convert.ToInt32(record["AssetID"]));
                    if (subAssetData != null)
                    {
                        foreach (SubAssetDataModel sub in subAssetData)
                        {
                            document.InsertSectionPageBreak();
                            text = reportFieldConfig.Evaluate + " " + reportFieldConfig.Audit + " " + reportFieldConfig.On + " " + sub.UserAssetID.ToString() + " " + reportFieldConfig.At + " " + record[Location].ToString();

                            Novacode.Paragraph Subheader = document.InsertParagraph(text, false, HeaderFormat).AppendLine();
                            Subheader.Alignment = Novacode.Alignment.center;
                            Novacode.Table SubDataTable = document.InsertTable(8, 3);
                            SubDataTable.Design = Novacode.TableDesign.TableGrid;
                            SubDataTable.Rows[0].Cells[0].Paragraphs.First().Append(reportFieldConfig.Name).Bold();
                            SubDataTable.Rows[0].Cells[1].Paragraphs.First().Append(reportFieldConfig.Value).Bold();
                            SubDataTable.Rows[0].Cells[0].Width = 150;
                            SubDataTable.Rows[0].Cells[1].Width = 270;
                            SubDataTable.Rows[0].Cells[2].Width = 250;

                            SubDataTable.MergeCellsInColumn(2, 0, 7);

                            List<string> SubImages = AddImages("SAST", Convert.ToInt32(sub.SubAssetID));

                            border = new Novacode.Border(Novacode.BorderStyle.Tcbs_none, Novacode.BorderSize.one, 1, System.Drawing.Color.White);

                            SubDataTable.Rows[0].Cells[2].SetBorder(Novacode.TableCellBorderType.Top, border);
                            SubDataTable.Rows[0].Cells[2].SetBorder(Novacode.TableCellBorderType.Right, border);
                            SubDataTable.Rows[0].Cells[2].SetBorder(Novacode.TableCellBorderType.Bottom, border);

                            if (SubImages != null)
                            {
                                //int imageIndex = 0;
                                foreach (var img in SubImages)
                                {
                                    Novacode.Paragraph paragraph = SubDataTable.Rows[0].Cells[2].InsertParagraph();
                                    paragraph.Alignment = Novacode.Alignment.center;
                                    Novacode.Image image = document.AddImage(img);
                                    paragraph.InsertPicture(image.CreatePicture()).AppendLine().Alignment = Novacode.Alignment.right;
                                    paragraph.Alignment = Novacode.Alignment.right;
                                    paragraph.IndentationHanging = 1.0f;
                                    paragraph.Direction = Novacode.Direction.RightToLeft;
                                }
                            }

                            //Novacode.Paragraph Label;
                            SubDataTable.Rows[1].Cells[0].Paragraphs.First().Append(reportFieldConfig.SubAssetID).Bold();
                            SubDataTable.Rows[1].Cells[1].Paragraphs.First().Append(sub.UserAssetID);

                            SubDataTable.Rows[2].Cells[0].Paragraphs.First().Append(reportFieldConfig.SubAssetType).Bold();
                            SubDataTable.Rows[2].Cells[1].Paragraphs.First().Append(sub.AssetTypeName);

                            SubDataTable.Rows[3].Cells[0].Paragraphs.First().Append(reportFieldConfig.SubAssetConditionRating).Bold();
                            SubDataTable.Rows[3].Cells[1].Paragraphs.First().Append(sub.ConditionRating);

                            SubDataTable.Rows[4].Cells[0].Paragraphs.First().Append(reportFieldConfig.SubAssetAreaLocated).Bold();
                            SubDataTable.Rows[4].Cells[1].Paragraphs.First().Append(sub.AreaLocated);

                            SubDataTable.Rows[5].Cells[0].Paragraphs.First().Append(reportFieldConfig.SubAssetMake).Bold();
                            SubDataTable.Rows[5].Cells[1].Paragraphs.First().Append(sub.Manufacturer);

                            SubDataTable.Rows[6].Cells[0].Paragraphs.First().Append(reportFieldConfig.SubAssetModel).Bold();
                            SubDataTable.Rows[6].Cells[1].Paragraphs.First().Append(sub.Model);

                            SubDataTable.Rows[7].Cells[0].Paragraphs.First().Append(reportFieldConfig.SubAssetSerial).Bold();
                            SubDataTable.Rows[7].Cells[1].Paragraphs.First().Append(sub.SerialNo);

                            for (int index = 1; index <= 7; index++)
                            {
                                SubDataTable.Rows[index].Cells[2].SetBorder(Novacode.TableCellBorderType.Top, border);
                                SubDataTable.Rows[index].Cells[2].SetBorder(Novacode.TableCellBorderType.Right, border);
                                SubDataTable.Rows[index].Cells[2].SetBorder(Novacode.TableCellBorderType.Bottom, border);
                            }
                        }
                    }// end of sub Assets
                    document.InsertSectionPageBreak();
                }
                //pdfRenderer.RenderDocument();
                tempFileName = "AssetReport" + DateTime.UtcNow.TimeOfDay.ToString().Replace(":", "").Replace("-", "").Replace(" ", "").Replace("/", "") + ".docx";
                document.SaveAs(Path.Combine(ReportOutputPath, tempFileName));
            }

            ReportFileName = tempFileName;
            //Removing temporary files
            //Directory.Delete(TempimagesPath, true);
            DeleteFiles(TempPath);
            return ReportFileName;
        }

        private List<string> AddImages(string Imagetype, int AssetID)
        {
            List<string> images = new List<string>();
            string Filename;
            SqlCommand ImageData;
            ImageData = sqlConn.CreateCommand();
            ImageData.CommandType = CommandType.StoredProcedure;
            ImageData.CommandText = "GetImageDataByTypeAndID";
            ImageData.Parameters.Add(new SqlParameter("ImageType", SqlDbType.VarChar)).Value = Imagetype;
            ImageData.Parameters.Add(new SqlParameter("AssociatedID", SqlDbType.Int)).Value = AssetID;

            using (SqlDataReader ImageDataReader = ImageData.ExecuteReader())
            {
                int ImageIndex = 1;
                while (ImageDataReader.Read())
                {
                    if (ImageIndex > 3) break;
                    //Path.Combine(ImageRootPath, ImageDataReader["ImagePath"].ToString()));
                    //Filename = Path.Combine(ImageRootPath, "Assets\\0\\" + ImageIndex.ToString() + ".jpg");
                    Filename = Path.Combine(ImageRootPath, ImageDataReader["ImagePath"].ToString());
                    if (System.IO.File.Exists(Filename))
                    {
                        string image = GetResizedImageFile(AssetID, ImageIndex, Filename);
                        if (image != "")
                        {
                            images.Add(image);
                            ImageIndex = ImageIndex + 1;
                        }
                    }
                }
            }
            return images;
        }
        private EmailTemplateEntityModel GetEmailTemplate(string templateName)
        {
            EmailTemplateEntityModel objEmailTemplateEntity = new EmailTemplateEntityModel();

            DataTable subData = new DataTable();
            SqlCommand SqlConn = sqlConn.CreateCommand();

            SqlConn.CommandType = CommandType.StoredProcedure;
            SqlConn.CommandText = "GetEmailTemplateByType";
            SqlConn.Parameters.Add("EmailType", SqlDbType.VarChar).Value = templateName;
            SqlDataReader DataReader = SqlConn.ExecuteReader();
            while (DataReader.Read())
            {
                var EmailTemplateEntity = new EmailTemplateEntityModel
                {
                    Body = DataReader["Body"].ToString(),
                    DataFields = DataReader["DataFields"].ToString(),
                    EmailTemplateID = Convert.ToInt32(DataReader["EmailTemplateID"].ToString()),
                    Subject = DataReader["Subject"].ToString(),
                    Type = DataReader["Type"].ToString(),

                };

                objEmailTemplateEntity = EmailTemplateEntity;
            }
            DataReader.Close();
            return objEmailTemplateEntity;

        }
        private List<SubAssetDataModel> GetReportSubAssetData(int AssetID)
        {
            DataTable subData = new DataTable();
            SqlCommand SqlConn = sqlConn.CreateCommand();

            SqlConn.CommandType = CommandType.StoredProcedure;
            SqlConn.CommandText = "GetReportSubAssetByAssetID";
            SqlConn.Parameters.Add("AssetID", SqlDbType.Int).Value = AssetID;
            SqlDataReader DataReader = SqlConn.ExecuteReader();
            List<SubAssetDataModel> SubAssetData = new List<SubAssetDataModel>();

            while (DataReader.Read())
            {
                //SubAssetDataModel subAsset = new SubAssetDataModel();
                var subAsset = new SubAssetDataModel
                {
                    SubAssetID = DataReader["SubAssetID"].ToString(),
                    AssetID = DataReader["AssetID"].ToString(),
                    UserAssetID = DataReader["UserAssetID"].ToString(),
                    AssetTypeID = DataReader["AssetTypeID"].ToString(),
                    AssetTypeName = DataReader["AssetTypeName"].ToString(),
                    ConditionRatingCd = DataReader["ConditionRatingCd"].ToString(),
                    ConditionRating = DataReader["ConditionRating"].ToString(),
                    AreaLocated = DataReader["AreaLocated"].ToString(),
                    Make = DataReader["Make"].ToString(),
                    Manufacturer = DataReader["Manufacturer"].ToString(),
                    Model = DataReader["Model"].ToString(),
                    SerialNo = DataReader["SerialNo"].ToString(),
                    HeightLength = DataReader["HeightLength"].ToString(),
                    WidthBreadth = DataReader["WidthBreadth"].ToString(),
                    ThicknessDepth = DataReader["ThicknessDepth"].ToString(),
                };
                SubAssetData.Add(subAsset);
            }
            return SubAssetData;
        }

        private List<AssetFieldListDetailsModel> ReadAssetFields(string LanguageLitral)
        {
            string SufixPath = @"AssetFields\" + LanguageLitral + ".json";
            String FilePath = Path.Combine(ConfigurationManager.AppSettings["LanguageConfigURL"].ToString(), SufixPath);
            List<AssetFieldListDetailsModel> LangDisplayList = new List<AssetFieldListDetailsModel>();
            using (StreamReader r = new StreamReader(FilePath))
            {
                string json = r.ReadToEnd();
                LangDisplayList = JsonConvert.DeserializeObject<List<AssetFieldListDetailsModel>>(json);
            }
            return LangDisplayList;
        }
        private ReportFieldConfig ReadReportFields(string LanguageLitral)
        {
            string SufixPath = @"ReportFields\" + LanguageLitral + ".json";
            String FilePath = Path.Combine(ConfigurationManager.AppSettings["LanguageConfigURL"].ToString(), SufixPath);
            ReportFieldConfig reportField = new ReportFieldConfig();
            using (StreamReader r = new StreamReader(FilePath))
            {
                string json = r.ReadToEnd();
                reportField = JsonConvert.DeserializeObject<ReportFieldConfig>(json);
            }
            return reportField;
        }

        private DataTable GetReportAssetData(int UserID, int CustomerID, DataTable AssetData, string AssetFields, string LanguageLitral)
        {
            SqlDataReader FieldReader;

            //List<AssetFieldListDetailsModel> AssetFields = new List<AssetFieldListDetailsModel>();

            List<string> BaseFields = new List<string>();
            List<string> CustomerFields = new List<string>();

            SqlCommand SqlCommand = sqlConn.CreateCommand();
            SqlCommand.CommandType = CommandType.StoredProcedure;
            SqlCommand.CommandText = "GetAllReportFormTemplates";
            SqlCommand.CommandTimeout = 180;
            //SqlConn.Parameters.Add("CustomerID", SqlDbType.Int).Value = 0;
            FieldReader = SqlCommand.ExecuteReader();

            System.Diagnostics.EventLog.WriteEntry("Report Manager Service", "Default Report templates", System.Diagnostics.EventLogEntryType.Information);

            while (FieldReader.Read())
            { BaseFields.Add(FieldReader["FormTemplate"].ToString()); }
            FieldReader.Close();

            List<AssetReportFieldModel> CustReportFields = new List<AssetReportFieldModel>();
            List<AssetReportFieldModel> assetReportFields = new List<AssetReportFieldModel>();

            foreach (var CurrItem in BaseFields)
            {
                IEnumerable<AssetReportFieldModel> tmp = JsonConvert.DeserializeObject<IEnumerable<AssetReportFieldModel>>(CurrItem);
                assetReportFields = assetReportFields.Union(tmp).ToList();
            }

            SqlCommand = sqlConn.CreateCommand();
            SqlCommand.CommandType = CommandType.StoredProcedure;
            SqlCommand.CommandText = "GetAllReportFormTemplates";
            SqlCommand.Parameters.Add("CustomerID", SqlDbType.Int).Value = CustomerID;
            FieldReader = SqlCommand.ExecuteReader();

            System.Diagnostics.EventLog.WriteEntry("Report Manager Service", "Customer Report templates", System.Diagnostics.EventLogEntryType.Information);

            if (FieldReader.HasRows)
            {
                // Only mandatory fields
                assetReportFields = assetReportFields.Where(x => x.DefaultBehaviour == "1").ToList();

                //Check for (Optional) additional Fields enabled for Customer
                while (FieldReader.Read())
                { CustomerFields.Add(FieldReader["FormTemplate"].ToString()); }
                FieldReader.Close();

                System.Diagnostics.EventLog.WriteEntry("Report Manager Service", "Has Custom Config - Row Count" + CustomerFields.Count.ToString(), System.Diagnostics.EventLogEntryType.Information);

                foreach (var CurrItem in CustomerFields)
                {
                    IEnumerable<AssetReportFieldModel> tmp = JsonConvert.DeserializeObject<IEnumerable<AssetReportFieldModel>>(CurrItem);
                    tmp = tmp.Where(x => x.DefaultBehaviour == "1").ToList();
                    assetReportFields = assetReportFields.Union(tmp).ToList();
                }

            }
            assetReportFields = assetReportFields.GroupBy(x => x.AssetFieldID).Select(g => g.First()).ToList();


            //Selecting fields as specified by the user from field AssetFields.
            if (AssetFields != "" && AssetFields != null)
            {
                string[] ArrSelectedFields = AssetFields.Split(',');
                if (!ArrSelectedFields.Contains("4"))
                {
                    List<string> arrFields = new List<string>();
                    arrFields = ArrSelectedFields.ToList();
                    arrFields.Add("4");
                    ArrSelectedFields = arrFields.ToArray();
                }
                if (!ArrSelectedFields.Contains("8"))
                {
                    List<string> arrFields = new List<string>();
                    arrFields = ArrSelectedFields.ToList();
                    arrFields.Add("8");
                    ArrSelectedFields = arrFields.ToArray();
                }
                assetReportFields = assetReportFields.Where(w => ArrSelectedFields.Any(city => city == w.AssetFieldID.ToString())).ToList();
            }
            //End

            System.Diagnostics.EventLog.WriteEntry("Report Manager Service", "Total fields after merging  Custom and Base - Row Count" + assetReportFields.Count.ToString(), System.Diagnostics.EventLogEntryType.Information);


            SqlCommand = sqlConn.CreateCommand();
            SqlCommand.CommandType = CommandType.StoredProcedure;
            SqlCommand.CommandText = "GetAssetFieldDetails";
            FieldReader = SqlCommand.ExecuteReader();

            List<AssetFieldListDetailsModel> AssetFieldList = new List<AssetFieldListDetailsModel>();

            while (FieldReader.Read())
            {
                AssetFieldListDetailsModel assetField = new AssetFieldListDetailsModel();
                assetField.AssetFieldID = Convert.ToInt32(FieldReader["AssetFieldID"]);
                assetField.FieldName = FieldReader["FieldNAme"].ToString();
                assetField.DisplayName = FieldReader["DisplayName"].ToString();
                assetField.Sequence = Convert.ToInt32(FieldReader["Sequence"]);
                AssetFieldList.Add(assetField);
            }
            FieldReader.Close();





            var query = from report in assetReportFields
                        join field in AssetFieldList
                             on report.AssetFieldID equals field.AssetFieldID

                        orderby field.Sequence
                        select new AssetFieldListDetailsModel
                        {
                            DefaultBehaviour = report.DefaultBehaviour,
                            DisplayName = field.DisplayName,
                            FieldName = field.FieldName,
                            Sequence = field.Sequence
                        };

            string[] additionalColumns = new[] {
                    "AssetID"};

            if (AssetFieldList != null && AssetFieldList.Count > 0)
            {
                List<AssetFieldListDetailsModel> tempQuery = new List<AssetFieldListDetailsModel>();
                tempQuery = query.ToList();
                List<AssetFieldListDetailsModel> LangDisplayList = ReadAssetFields(LanguageLitral);
                foreach (var item in LangDisplayList)
                {
                    tempQuery.ToList().Where(w => w.FieldName == item.FieldName).ToList().ForEach(x => x.DisplayName = item.DisplayName);
                }

                query = tempQuery;
            }


            System.Diagnostics.EventLog.WriteEntry("Report Manager Service", "Joined Field Details: " + query.ToList().ToString(), System.Diagnostics.EventLogEntryType.Information);

            //Configuring columns to show
            string[] selectedColumns = query.ToList().Where(y => y.FieldName != "Photos" && y.FieldName != "QuoteReqd").Select(x => x.FieldName).ToArray();

            System.Diagnostics.EventLog.WriteEntry("Report Manager Service", "Columns: " + selectedColumns.ToString(), System.Diagnostics.EventLogEntryType.Information);


            var AllColumns = new string[selectedColumns.Length + additionalColumns.Length];
            selectedColumns.CopyTo(AllColumns, 0);
            additionalColumns.CopyTo(AllColumns, selectedColumns.Length);
            selectedColumns = AllColumns;
            System.Diagnostics.EventLog.WriteEntry("Report Manager Service", "Total Columns length: " + selectedColumns.Length.ToString(), System.Diagnostics.EventLogEntryType.Information);

            DataTable dt = new DataView(AssetData).ToTable(false, selectedColumns);

            System.Diagnostics.EventLog.WriteEntry("Report Manager Service", "Net Fields : " + selectedColumns.Length.ToString(), System.Diagnostics.EventLogEntryType.Information);

            foreach (var item in query.ToList().Where(y => y.FieldName != "Photos" && y.FieldName != "QuoteReqd"))
            {
                if (dt.Columns.Contains(item.FieldName))
                {
                    dt.Columns[item.FieldName].ColumnName = item.DisplayName;

                }
            }

            System.Diagnostics.EventLog.WriteEntry("Report Manager Service", "Row coun: " + dt.Rows.Count.ToString(), System.Diagnostics.EventLogEntryType.Information);
            // dt.Rows.Count.ToString();
            return dt;

            //using (IDataReader reader = Reportcomm.ExecuteReader())
            //{
            //    List<Fields> AssetFields = reader.Select(r => new Fields
            //    {
            //        FieldID = r["FieldID"] is DBNull ? 0 : Convert.ToInt32(r["FieldID"].ToString()),
            //        FieldName = r["FieldName"] is DBNull ? "N/A" : Convert.ToString(r["FieldName"].ToString()),
            //        DisplayName = r["DisplayName"] is DBNull ? "N/A" : Convert.ToString(r["DisplayName"].ToString()),
            //    }).ToList();
            //    if (AssetFields != null)
            //    {
            //        return AssetFields;
            //    }
            //    else
            //    {
            //        return null;
            //    }
            //}
        }

        static byte[] GetBytes(string str)
        {
            //NOT IN USE
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }
        private string GetResizedImageFile(int AssetID, int imageIndex, string filePathName)
        {
            String NewImageFileName = Path.Combine(TempPath, AssetID.ToString() + imageIndex.ToString() + "f.jpg");
            System.Drawing.Image finalImage;
            try
            {
                using (System.Drawing.Image image = System.Drawing.Image.FromFile(filePathName))
                {
                    using (finalImage = ImageManager.ResizeImage(image, 200, 250, true))
                    {
                        finalImage.Save(NewImageFileName);
                    }
                }
                finalImage.Dispose();
            }
            catch (Exception ex)
            {
                NewImageFileName = "";
            }
            return NewImageFileName;
        }


        private SqlDataReader GetReports(string Status)
        {
            try
            {
                using (EventLog eventLog = new EventLog("Application"))
                {
                    eventLog.Source = "Application";
                    eventLog.WriteEntry("Inside getreports", EventLogEntryType.SuccessAudit, 101, 1);
                }
                SqlCommand Reportcomm = sqlConn.CreateCommand();
                Reportcomm.CommandType = CommandType.StoredProcedure;
                Reportcomm.CommandText = "GetAssetReports";
                Reportcomm.Parameters.Add("Status", SqlDbType.VarChar).Value = Status;
                return Reportcomm.ExecuteReader();
            }
            catch (Exception ex)
            {
                using (EventLog eventLog = new EventLog("Application"))
                {
                    eventLog.Source = "Application";
                    eventLog.WriteEntry("Error getreports" + ex.Message, EventLogEntryType.SuccessAudit, 101, 1);
                }
                return null;
            }

        }
        private async void StartReportManager()
        {
            try
            {


                using (EventLog eventLog = new EventLog("Application"))
                {
                    eventLog.Source = "Application";
                    eventLog.WriteEntry("StartReportManager start", EventLogEntryType.SuccessAudit, 101, 1);
                }
                int userID, customerID, AssetReportID;
                string FileName, Format;
                int? locationID = null;
                string ConditionRating, AssetStatus, categoryCD, Criticality,
                    Ownership, AssetType, Refrigerant, FiltersFitted,
                    ReplacementYear, AgeGroup, RemainingLife, ReplacementCost,
                    OHSComments, JobCount, AssetFields, State, Language;
                while (true)
                {
                    using (EventLog eventLog = new EventLog("Application"))
                    {
                        eventLog.Source = "Application";
                        eventLog.WriteEntry("Before getreports start", EventLogEntryType.SuccessAudit, 101, 1);
                    }
                    SqlDataReader Reports = GetReports("Queued"); //Reportcomm.ExecuteReader();

                    using (EventLog eventLog = new EventLog("Application"))
                    {
                        eventLog.Source = "Application";
                        eventLog.WriteEntry("After getreports Field Count : ", EventLogEntryType.SuccessAudit, 101, 1);
                    }
                    while (Reports.Read())
                    {
                        ConditionRating = AssetStatus = categoryCD = Criticality = Ownership = AssetType
                        = Refrigerant = FiltersFitted = ReplacementYear = AgeGroup = RemainingLife = ReplacementCost
                        = JobCount = OHSComments = AssetFields = State = Language = String.Empty;
                        AssetReportID = Convert.ToInt16(Reports["AssetReportID"]);
                        userID = Convert.ToInt16(Reports["UserID"].ToString());
                        customerID = Convert.ToInt16(Reports["CustomerID"].ToString());
                        Format = Convert.ToString(Reports["Format"]).Trim().ToUpper();
                        if (!Reports.IsDBNull(Reports.GetOrdinal("LocationID")))
                            locationID = Convert.ToInt16(Reports["LocationID"]);
                        if (!string.IsNullOrEmpty(Reports["ConditionRating"].ToString()))
                            ConditionRating = Convert.ToString(Reports["ConditionRating"]);
                        if (!string.IsNullOrEmpty(Reports["AssetStatus"].ToString()))
                            AssetStatus = Convert.ToString(Reports["AssetStatus"]);
                        if (!string.IsNullOrEmpty(Reports["AssetCategory"].ToString()))
                            categoryCD = Convert.ToString(Reports["AssetCategory"]);
                        if (!string.IsNullOrEmpty(Reports["Criticality"].ToString()))
                            Criticality = Convert.ToString(Reports["Criticality"]);
                        if (!string.IsNullOrEmpty(Reports["Ownership"].ToString()))
                            Ownership = Convert.ToString(Reports["Ownership"]);
                        if (!string.IsNullOrEmpty(Reports["AssetType"].ToString()))
                            AssetType = Convert.ToString(Reports["AssetType"]);
                        if (!string.IsNullOrEmpty(Reports["Refrigerant"].ToString()))
                            Refrigerant = Convert.ToString(Reports["Refrigerant"]);
                        if (!string.IsNullOrEmpty(Reports["FiltersFitted"].ToString()))
                            FiltersFitted = Convert.ToString(Reports["FiltersFitted"]);
                        if (!string.IsNullOrEmpty(Reports["ReplacementYear"].ToString()))
                            ReplacementYear = Convert.ToString(Reports["ReplacementYear"]);
                        if (!string.IsNullOrEmpty(Reports["AgeGroup"].ToString()))
                            AgeGroup = Convert.ToString(Reports["AgeGroup"]);
                        if (!string.IsNullOrEmpty(Reports["RemainingLife"].ToString()))
                            RemainingLife = Convert.ToString(Reports["RemainingLife"]);
                        if (!string.IsNullOrEmpty(Reports["ReplacementCost"].ToString()))
                            ReplacementCost = Convert.ToString(Reports["ReplacementCost"]);
                        if (!string.IsNullOrEmpty(Reports["JobCount"].ToString()))
                            JobCount = Convert.ToString(Reports["JobCount"]);
                        if (!string.IsNullOrEmpty(Reports["OHSComments"].ToString()))
                            OHSComments = Convert.ToString(Reports["OHSComments"]);
                        if (!string.IsNullOrEmpty(Reports["AssetFields"].ToString()))
                            AssetFields = Convert.ToString(Reports["AssetFields"]);
                        if (!string.IsNullOrEmpty(Reports["State"].ToString()))
                            State = Convert.ToString(Reports["State"]);
                        if (!string.IsNullOrEmpty(Reports["Language"].ToString()))
                            Language = Convert.ToString(Reports["Language"]);
                        else
                            Language = "en";


                        using (EventLog eventLog = new EventLog("Application"))
                        {
                            eventLog.Source = "Application";
                            eventLog.WriteEntry("Fill Report Data", EventLogEntryType.SuccessAudit, 101, 1);
                        }
                        try
                        {
                            UpdateReport(AssetReportID, "In Progress");
                            Task<string> task = GenerateReport(Format, userID, customerID, locationID, AssetReportID,
                                ConditionRating, AssetStatus, categoryCD, Criticality
                                , Ownership, AssetType, Refrigerant, FiltersFitted, ReplacementYear
                                , AgeGroup, OHSComments, RemainingLife, ReplacementCost, JobCount, AssetFields, State, Language);
                            FileName = await task;
                            UpdateReport(AssetReportID, "Processed", FileName);
                        }
                        catch (Exception ex)
                        {
                            string errmsg;
                            errmsg = "Error(s) encountered while Processing Asset Report ID :" + AssetReportID.ToString() + Environment.NewLine + Environment.NewLine;
                            errmsg += "Error message details " + Environment.NewLine;
                            errmsg += ex.ToString();
                            System.Diagnostics.EventLog.WriteEntry("Report Manager Service", errmsg, System.Diagnostics.EventLogEntryType.Error);
                            UpdateReport(AssetReportID, "Processing-Error");
                        }
                    }
                    Thread.Sleep(1000 * 260);
                }
            }
            catch (Exception ex)
            {

                using (EventLog eventLog = new EventLog("Application"))
                {
                    eventLog.Source = "Application";
                    eventLog.WriteEntry("StartReportManager Exception " + ex.StackTrace + " " + ex.Message, EventLogEntryType.SuccessAudit, 101, 1);
                }
            }
        }

        private void StartEmailer()
        {
            int AssetReportID;
            string UserName, FileName, UserEmail;
            string Customer;
            string Location;
            DateTime ReportDate;
            while (true)
            {
                SqlDataReader ReportsData = GetReports("Processed");
                while (ReportsData != null && ReportsData.Read())
                {
                    AssetReportID = Convert.ToInt32(ReportsData["AssetReportID"]);
                    UserName = ReportsData["FirstName"].ToString() + ReportsData["LastName"].ToString();
                    UserEmail = ReportsData["EmailID"].ToString();
                    FileName = ReportsData["ReportName"].ToString();
                    Customer = ReportsData["CustomerName"].ToString();
                    Location = ReportsData["LocationName"].ToString();
                    ReportDate = Convert.ToDateTime(ReportsData["CreationDate"]);

                    //if (Mode.ToUpper() == "TEST")
                    //    UserEmail = "kuljot.singh@eudemonic.co.in";

                    try
                    {
                        if (SendReport(UserName, UserEmail, Customer, Location, String.Format("{0:dd-MM-yyyy HH:mm:ss}", ReportDate), FileName))
                            UpdateReport(AssetReportID, "Sent");
                    }
                    catch (Exception ex)
                    {
                        string errmsg;
                        errmsg = "Error(s) encountered while sending Email Asset Report ID :" + AssetReportID.ToString() + Environment.NewLine + Environment.NewLine;
                        errmsg += "Error message details " + Environment.NewLine;
                        errmsg += ex.ToString();
                        System.Diagnostics.EventLog.WriteEntry("Report Manager Service", errmsg, System.Diagnostics.EventLogEntryType.Error);
                        UpdateReport(AssetReportID, "Email-Error");
                    }
                }
                Thread.Sleep(1000 * 460);
            }
        }
        private bool SendReport(string UserName, string UserEmail, string Customer, string Location, string ReportDate, string FileName)
        {
            string emailbody;
            EmailTemplateEntityModel EmailTemplateEntity = GetEmailTemplate("PortfolioReport");
            emailbody = EmailTemplateEntity.Body;
            emailbody = emailbody.Replace("<UserName>", UserName);
            emailbody = emailbody.Replace("<Customer>", Customer);
            emailbody = emailbody.Replace("<Location>", Location);
            emailbody = emailbody.Replace("<ReportDate>", ReportDate);
            emailbody = emailbody.Replace("<ReportURL>", ReportURL + FileName);
            MailMessage mail = new MailMessage();
            SmtpClient client = new SmtpClient();
            mail.From = new MailAddress(Senderemail);
            mail.To.Add(UserEmail);
            mail.Subject = EmailTemplateEntity.Subject;
            mail.Body = emailbody;
            mail.IsBodyHtml = true;
            client.Send(mail);
            return true;
        }

        public static void DeleteFiles(string DirectoryPath)
        {
            //delete files:
            System.IO.DirectoryInfo directory = new System.IO.DirectoryInfo(DirectoryPath);
            foreach (System.IO.FileInfo file in directory.GetFiles())
                file.Delete();
            //delete directories in this directory:
            foreach (System.IO.DirectoryInfo subDirectory in directory.GetDirectories())
                directory.Delete(true);
        }

        private void UpdateReport(int ReportID, string status, string ReportName = "")
        {
            SqlCommand sqlcmd = sqlConn.CreateCommand();
            sqlcmd.Connection = sqlConn;
            string sql;
            sql = "Update AssetReport set status='" + status + "'";
            if (ReportName != "") sql += " , ReportName='" + ReportName + "'";
            sql += " where AssetReportID=" + ReportID.ToString();
            sqlcmd.CommandText = sql;
            sqlcmd.ExecuteScalar();
        }
    }
    public static class Extensionmethod
    {
        public static IEnumerable<T> Select<T>(this IDataReader reader,
                                        Func<IDataReader, T> projection)
        {
            while (reader.Read())
            {
                yield return projection(reader);
            }
        }
    }
    public class Fields
    {
        public int FieldID { get; set; }
        public string FieldName { get; set; }
        public string DisplayName { get; set; }
    }
}