﻿using System;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;

namespace UtilityBox
{
    static class ImageManager
    {
        public static void Base64StringToImage(string FileNamePath, string content)
        {
            content = content.Replace("data:image/jpg;base64,", "").Replace("data:image/jpeg;base64,","").Replace("data:image/JPG;base64,", "").Replace("data:image/JPEG;base64,", "");
            var bytes = Convert.FromBase64String(content);
            using (var imageFile = new FileStream(FileNamePath, FileMode.Create))
            {
                imageFile.Write(bytes, 0, bytes.Length);
                imageFile.Flush();
            }
        }

        //public static Image ResizeImage(Image image,int newWidth, int newHeight)
        //{
        //    Bitmap newImage = new Bitmap(newWidth, newHeight);

        //    using (Graphics graphicsHandle = Graphics.FromImage(newImage))
        //    {
        //        graphicsHandle.InterpolationMode = InterpolationMode.HighQualityBicubic;
        //        graphicsHandle.CompositingQuality = CompositingQuality.HighQuality;
        //        graphicsHandle.SmoothingMode = SmoothingMode.HighQuality;
        //        graphicsHandle.PixelOffsetMode = PixelOffsetMode.HighQuality;
        //        graphicsHandle.DrawImage(image, 0, 0, newWidth, newHeight);
        //    }
        //    return newImage;
        //}

        public static Image ResizeImage(Image image, int MaxWidth=0, int MaxHeight = 0, 
            bool LockAspectRation = true)
        {
            //foreach (var prop in image.PropertyItems)
            //{

            //    if ((prop.Id == 0x0112 || prop.Id == 5029 || prop.Id == 274))
            //    {
            //        var value = (int)prop.Value[0];

            //        if (value == 1)
            //        {
            //            //Normal Horizantal...no change required
            //            break;
            //        }

            //        if (value == 6)
            //        {
            //            newWidth = 480;
            //            newHeight = 640;
            //            image.RotateFlip(RotateFlipType.Rotate90FlipNone);
            //            break;
            //        }
            //        else if (value == 8)
            //        {
            //            newWidth = 480;
            //            newHeight = 640;
            //            image.RotateFlip(RotateFlipType.Rotate270FlipNone);
            //            break;
            //        }
            //        else if (value == 3)
            //        {
            //            newWidth = 640;
            //            newHeight = 480;
            //            image.RotateFlip(RotateFlipType.Rotate180FlipNone);
            //            break;
            //        }
            //    }
            //}

            int newWidth =0;
            int newHeight =0;
            int fraction = 0;

            if (image.Width < MaxWidth)  // 150 < 200 
                MaxWidth = image.Width;

            if (image.Height < MaxHeight)
                MaxHeight = image.Height;

            if (LockAspectRation)
            {
                if (image.Width > image.Height)
                {
                    newWidth = MaxWidth;
                    fraction = (image.Width - newWidth) * 100 / image.Width;
                    newHeight = image.Height - (fraction * image.Height) / 100;

                }
                else
                {
                    newHeight = MaxHeight;
                    fraction = (image.Height - newHeight) * 100 / image.Height;
                    newWidth = image.Width - (fraction * image.Width) / 100;
                }
            }
            else
            {
                newWidth = MaxWidth;
                newHeight = MaxHeight;
            }

            Bitmap newImage = new Bitmap(newWidth, newHeight);

            using (Graphics graphicsHandle = Graphics.FromImage(newImage))
            {
                graphicsHandle.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphicsHandle.CompositingQuality = CompositingQuality.HighQuality;
                graphicsHandle.SmoothingMode = SmoothingMode.HighQuality;
                graphicsHandle.PixelOffsetMode = PixelOffsetMode.HighQuality;
                graphicsHandle.DrawImage(image, 0, 0, newWidth, newHeight);
            }
            return newImage;
        }

    }
}
