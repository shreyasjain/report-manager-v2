﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Report_manager_app
{
    public partial class AddInstanceForm : Form
    {
        ListInstanceForm owner;
        int PInstanceID = 0;
        public AddInstanceForm()
        {
            InitializeComponent();
        }
        public AddInstanceForm(ListInstanceForm listInstanceForm)
        {
            InitializeComponent();
            owner = listInstanceForm;
        }

        public void BindData(int InstanceID)
        {
            PInstanceID = InstanceID;

            if (PInstanceID > 0)
            {
                Common cs = new Common();
                ConfigModel config = cs.ReadConfig();
                InstanceModel reportManager = config.Instances.Where(x => x.InstanceID == InstanceID).FirstOrDefault();
                reportManager.InstanceID = InstanceID;
                txtTemplatePath.Text = reportManager.TemplatePath;
                txtTempPath.Text = reportManager.TempPath;
                txtInstanceName.Text = reportManager.InstanceName;
                txtConnectionString.Text = reportManager.ConnectionString;
                txtImageRoutePath.Text = reportManager.ImageRoutePath;
                txtReportOutputPath.Text = reportManager.ReportOutputPath;



            }
        }
        private bool WithErrors()
        {
            if (txtConnectionString.Text.Trim() == String.Empty)
                return true; // Returns true if no input or only space is found
            if (txtInstanceName.Text.Trim() == String.Empty)
                return true;
            if (txtImageRoutePath.Text.Trim() == String.Empty)
                return true;

            if (txtTemplatePath.Text.Trim() == String.Empty)
                return true;

            if (txtTempPath.Text.Trim() == String.Empty)
                return true;
            if (txtReportOutputPath.Text.Trim() == String.Empty)
                return true;


            return false;
        }


        private bool ValidDirectory(string path)
        {
            if (Directory.Exists(path))
            {
                return true;
            }
            else
            {
                MessageBox.Show("Invalid Directory, Please enter correct folder path","Error",MessageBoxButtons.OK,MessageBoxIcon.Error);
                return false;
            }
        }

        private void btnAddButton_Click(object sender, EventArgs e)
        {
            
            if (!WithErrors() && ValidDirectory(txtImageRoutePath.Text.Trim())
                 && ValidDirectory(txtTemplatePath.Text.Trim())
                 && ValidDirectory(txtTempPath.Text.Trim())
                 && ValidDirectory(txtReportOutputPath.Text.Trim())
                 )
            {
                if (!TestConnectionString(txtConnectionString.Text.Trim()))
                {
                MessageBox.Show("Invalid Connection string, Please enter a valid connection string","Error",MessageBoxButtons.OK,MessageBoxIcon.Error);
                    return;
                }


                    Common cs = new Common();
                ConfigModel config = cs.ReadConfig();
                InstanceModel reportManager = new InstanceModel();
                reportManager.ConnectionString = txtConnectionString.Text.Trim();
                reportManager.ImageRoutePath = txtImageRoutePath.Text.Trim();
                reportManager.InstanceName = txtInstanceName.Text.Trim();
                reportManager.TemplatePath = txtTemplatePath.Text.Trim();
                reportManager.TempPath = txtTempPath.Text.Trim();
                reportManager.ReportOutputPath = txtReportOutputPath.Text.Trim();
                reportManager.Status = InstanceStatus.Stopped;

                //Save Database code 
                if (PInstanceID > 0)
                {
                    reportManager.InstanceID = PInstanceID;
                    int currIndex = config.Instances.FindIndex(ind => ind.InstanceID == PInstanceID);
                    config.Instances.RemoveAt(currIndex);
                    config.Instances.Insert(currIndex, reportManager);

                
                    MessageBox.Show("Instance updated successfully", "Success!", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
                else
                {
                    reportManager.InstanceID = config.Instances.OrderByDescending(x => x.InstanceID).FirstOrDefault() != null ? config.Instances.OrderByDescending(x => x.InstanceID).FirstOrDefault().InstanceID + 1 : 1;
                    config.Instances.Add(reportManager);
                    MessageBox.Show("Instance added successfully", "Success!", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
                cs.UpdateConfig(config);
                Common.UpdateInstanceStatus(InstanceStatus.Stopped, "Success", reportManager);

                this.Hide();
                owner.BindGrid();
            }
            else
            {
                MessageBox.Show("Please fill all mandatory fields", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }

        private void btnTestConnection_Click(object sender, EventArgs e)
        {
            if (txtConnectionString.Text.Trim() != "")
            {
                if (TestConnectionString(txtConnectionString.Text.Trim()))
                {
                    MessageBox.Show("Connection is valid.");
                }
                else
                {
                    MessageBox.Show("Please enter a valid connection string.");

                }
            }
        }
        public bool TestConnectionString(string ConnectionString)
        {
            try
            {
                string provider = "System.Data.SqlClient"; // for example
                DbProviderFactory factory = DbProviderFactories.GetFactory(provider);
                using (DbConnection conn = factory.CreateConnection())
                {
                    conn.ConnectionString = ConnectionString;
                    conn.Open();
                    return true;
                }
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("TestConnectionString ", ex.Message + "Trace" + ex.StackTrace, EventLogEntryType.Error, 121, short.MaxValue);

                return false;
            }
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            txtConnectionString.Text = "";
            txtImageRoutePath.Text = "";
            txtInstanceName.Text = "";
            txtTemplatePath.Text = "";
            txtTempPath.Text = "";
            txtReportOutputPath.Text = "";
        }

        private void AddInstanceForm_Load(object sender, EventArgs e)
        {

        }
    }
}
